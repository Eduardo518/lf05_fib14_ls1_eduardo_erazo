//===================Ausgabeformatierung 1====================
public class KonsoleAusgabe {
	
	public static void main(String[] args) {
		
		String satz1= "Das ist ein Beispielsatz";
		String satz2=" Ein Beispielsatz ist das.";
		
		System.out.print("\""+satz1+"\" "+ satz2);
		/*das ln bei println() steht f�r Zeile. Das bedeutet,dass sich die neue Ausgabe in einer neuen Zeile befindet,
		 also mit einem Zeilenumbruch/Absatz. 
        Bei print() Wird in der aktuellen Zeile weitergeschrieben.*/
		
		System.out.print("\n");
		System.out.print("\n");
		
		//======================================
		//AUFGABE 2:
		
		System.out.printf( "%7s\n","*");
		System.out.printf( "%8s\n","***");
		System.out.printf( "%9s\n","*****");
		System.out.printf( "%10s\n"," *******");
		System.out.printf( "%11s\n","*********");
		System.out.printf( "%12s\n","***********");
		System.out.printf( "%13s\n","*************");
		System.out.printf( "%8s\n","***");
		System.out.printf( "%8s\n","***");
		
		
		//================================================
		//AUFGABE 3
		double a = 22.4234234;
		double b= 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf( "%.2f\n" ,a);
		System.out.printf( "%.2f\n" ,b);
		System.out.printf( "%.2f\n" ,c);
		System.out.printf( "%.2f\n" ,d);
		System.out.printf( "%.2f\n" ,e);
		//==================

			//=====


	}

}
