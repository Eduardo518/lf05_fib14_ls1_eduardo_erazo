﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
            
       double zuZahlenderBetrag;              
       Scanner tastatur = new Scanner(System.in);
       boolean mehrTickets= true;
       
       while(mehrTickets) {  
    	   
    	   System.out.print("\n--------------------------------------\n");    	   
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
           double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
           rueckgeldAusgeben(rückgabebetrag);
           
           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir wünschen Ihnen eine gute Fahrt."); 
           
           System.out.print("\nMöchten Sie eine neue Fahrkarte kaufen ? \n"+
                   "j - Ja \n" +
        		   "n - Nein \n");          
           
           char option = tastatur.next().charAt(0); 
           
           if (option == 'j') mehrTickets = true;
           else if  (option == 'n') mehrTickets = false;
           else  System.out.print("\nUngültige option\n");
       	 }    	       	   
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0.00;
    	int anzahlTickets;
    	boolean gueltigeOption = false;
    	
    	while (!gueltigeOption) {
    		
        	System.out.println("\nEinzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"+
                    "Tageskarte Regeltarif AB [8,60 EUR] (2)\n"+
                    "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n"); 
      	 System.out.println("\n Ihre Wahl:");
      	 int option = tastatur.nextInt();
      	 
      	 switch (option) {
      	 
      	 case 1:
      		 zuZahlenderBetrag = 2.90; 
      		 gueltigeOption = true;
      		 break;
      		 
      	 case 2:
      		 zuZahlenderBetrag = 8.60; 
      		 gueltigeOption = true;
      		 break;
      		 
      	 case 3:
      		 zuZahlenderBetrag = 23.50;
      		 gueltigeOption = true;
      		 break;
      		 
           default:
          	System.out.println("\n>>Falsche Eingabe<<\n");
          	gueltigeOption = false;
              break;         		 		 
      	 }
    		
    	}
    	 
        System.out.print("Anzahl der Tickets:");
        anzahlTickets = tastatur.nextInt();

        return zuZahlenderBetrag * anzahlTickets;  	
    }
    
    public static double fahrkartenBezahlen (double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	Scanner tastatur = new Scanner(System.in);
    	double rückgabebetrag;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
        	
     	   System.out.printf("Noch zu zahlen: %.2f \n " ,(zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;          
        }
        
        fahrkarteAusgeben();
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rückgabebetrag;
    	
    }
    public static void rueckgeldAusgeben (double rückgabebetrag ) {
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
              muenzeAusgeben(2, "EURO");
              rückgabebetrag -= 2.0;
              
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  muenzeAusgeben(1, "EURO");  
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {  	
              muenzeAusgeben(50, "CENT");  
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
              muenzeAusgeben(20, "CENT");        	  
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
              muenzeAusgeben(10, "CENT");         	  
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
              muenzeAusgeben(5, "CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    	
    }
    public static void fahrkarteAusgeben () {
    	 System.out.println("\nFahrschein wird ausgegeben");
         
         int millisekunde = 250;
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            warte(millisekunde);
         }
         System.out.println("\n\n");        
    }
    
    public static void warte(int millisekunde) {
        try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
   	  System.out.println(betrag + " "+ einheit);
        
    	
    }
}