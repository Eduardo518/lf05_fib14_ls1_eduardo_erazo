import java.util.Scanner;

public class Funktionsl�ser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		final double eulersche = 2.718;
		System.out.print("Geben Sie einen Wert f�r X ein: ");
		double x = tastatur.nextDouble();
		double erg;
		
		if (x <= 0) {
		    erg = Math.pow(eulersche, x);
			System.out.print(" \nFunktionswert:" + erg + "---" + "exponentiell");
		}
		else if (x > 0 && x <= 3) {
			erg = Math.pow(x, 2) + 1;
			System.out.print(" \nFunktionswert:" + erg + "---" + "quadratisch");			
		}
		else if (x > 3) {
			erg = 2*x+4;
			System.out.print(" \nFunktionswert:" + erg + "---" + "linear");	
		}
	}
}
