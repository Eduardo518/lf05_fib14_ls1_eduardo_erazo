import java.util.Scanner;

public class RoemischeZahlen {
	/*Erstellen Sie die Konsolenanwendung Rom. Das Programm Rom soll nach der Eingabe 
eines r�mischen Zahlzeichens die entsprechende Dezimalzahl ausgeben (I = 1, V = 5, X = 
10, L = 50, C = 100, D = 500, M = 1000). Falls ein anderes Zeichen eingegeben wird, soll ein 
entsprechender Hinweis ausgegeben werden.*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie eine roemische Anzahl ein:");
		char roemisheZeichen = tastatur.next().charAt(0);
		
		if (roemisheZeichen == 'I') {
			System.out.print("\ndas Zahlzeichen I entspricht: " + 1);			
		}
		else if(roemisheZeichen == 'V') {
			System.out.print("\ndas Zahlzeichen V entspricht: " + 5);		
			
		}
		else if(roemisheZeichen == 'X') {
			System.out.print("\ndas Zahlzeichen X entspricht: " + 10);		
			
		}
		else if(roemisheZeichen == 'L') {
			System.out.print("\ndas Zahlzeichen L entspricht: " + 50);		
			
		}
		else if(roemisheZeichen == 'C') {
			System.out.print("\ndas Zahlzeichen C entspricht: " + 100);		
			
		}
		else if(roemisheZeichen == 'D') {
			System.out.print("\ndas Zahlzeichen D entspricht: " + 500);		
			
		}
		else if(roemisheZeichen == 'M') {
			System.out.print("\ndas Zahlzeichen M entspricht: " + 1000);		
			
		}
		else {
			System.out.print("\nEingabe ung�ltig!");
		}

	}

}
