import java.util.Scanner;

public class Taschenrechner {
	/*Der Benutzer soll zwei Zahlen in das Programm eingeben, danach soll er entscheiden, ob die 
Zahlen addiert, subtrahiert, multipliziert oder dividiert werden, diese Entscheidung soll �ber die 
Eingabe der folgenden Symbole get�tigt werden: +, -, *, /
Nach der Auswahl soll das Ergebnis der Rechnung ausgegeben werden, bzw. eine 
Fehlermeldung, falls eine falsche Auswahl getroffen wurde.*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner tastatur = new Scanner (System.in);
		System.out.print("Geben Sie ein Zahl ein: ");
		double zahl1 = tastatur.nextDouble();
		System.out.print("\nGeben Sie eine andere Zahl ein:");
		double zahl2 = tastatur.nextDouble();
		double erg;

		System.out.print("\nGeben Sie ein Symbol ein (+,-,*,/): ");
		char symbol = tastatur.next().charAt(0);
		
		if (symbol == '+'){
			 erg = zahl1+zahl2;
			System.out.print("\n" + zahl1 + " + " + zahl2 + " = " + erg);
			
		}
		else if (symbol == '-') {
			 erg = zahl1-zahl2;
			System.out.print("\n" + zahl1 + " - " + zahl2 + " = " + erg);
			
		}
		else if (symbol == '*') {
			 erg = zahl1*zahl2;
			System.out.print("\n" + zahl1 + " * " + zahl2 + " = " + erg);
			
		}
		else if (symbol == '/') {
			 erg = zahl1/zahl2;
			System.out.print("\n" + zahl1 + " / " + zahl2 + " = " + erg);			
		}
		else {
			System.out.print("\nEingabe ung�ltig");
		}

	}

}
