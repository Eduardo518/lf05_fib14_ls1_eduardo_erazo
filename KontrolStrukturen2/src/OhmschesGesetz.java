import java.util.Scanner;

public class OhmschesGesetz {
	/* Nach dem Ohmschen Gesetz berechnet sich der Widerstand eines ohmschen Widerstandes 
mit:
𝑅 =𝑈/𝐼
Schreiben Sie ein Programm, in das der Benutzer zunächst über die Eingabe der 
Buchstaben R, U oder I auswählen kann, welche Größe berechnet werden soll. Gibt er einen 
falschen Buchstaben ein, soll eine Meldung über die Fehleingabe erfolgen.
Anschließend soll er die Werte der fehlenden Größen eingeben. Am Ende gibt das 
Programm den Wert der gesuchten Größe mit der richtigen Einheit aus.*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Waehlen Sie aus, was Sie berechnen moechte: R - Widerstand, U - Spannung, I - Strom ");
		char  option = tastatur.next().charAt(0);
		double u;
		double i;
		double r;
		
		if (option == 'R') {
		   System.out.print("\nGeben Sie bitte die Spannung (U) ein: ");
		   u = tastatur.nextDouble();
		   System.out.print("\nGeben Sie bitte die Strom (I) ein: ");
		   i = tastatur.nextDouble();
		   
		   r = u/i; 
		   System.out.print("\n Widerstand = "+ r);		   
		}
		else if (option == 'U') {		
			
			System.out.print("\nGeben Sie bitte die Widerstand (R) ein: ");
		    r = tastatur.nextDouble();
			System.out.print("\nGeben Sie bitte die Strom (I) ein: ");
			i = tastatur.nextDouble();
			
			u= r*i;
			System.out.print("\n Spannung = "+ u);	
			
		}
		
		else if (option == 'I') {		
			
			System.out.print("\nGeben Sie bitte die Spannung (U) ein: ");
		    u = tastatur.nextDouble();
			System.out.print("\nGeben Sie bitte die Wiedestand (R) ein: ");
			r = tastatur.nextDouble();
			
			i= u/r;
			System.out.print("\n Strom = "+ i);	
			
		}
		

	}

}
