import java.util.Scanner;

public class EigeneBedingungen {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Geben Sie einen Zahl ein: ");
		int zahl1 = scanner.nextInt();
		
		System.out.print("Geben Sie einen anderen Zahl ein: ");
		int zahl2 = scanner.nextInt();
		
		if (zahl1 == zahl2) {
			System.out.print(zahl1 + " ist gleich " + zahl2);
		}

		else if(zahl2 > zahl1) {
			
			System.out.print(zahl2 + " ist groesser als "+ zahl1);
		}
		else if (zahl1 >= zahl2 ) {
			System.out.print(zahl1 + " ist groesser als " +zahl2 );
		}
		
		
	}

}
