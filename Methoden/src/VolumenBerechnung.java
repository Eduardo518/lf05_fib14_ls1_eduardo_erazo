
public class VolumenBerechnung {

	public static void main(String[] args) {
		double a=5.2;
		double b=4.5;
		double c=2.5;
		double h=3.4;
		double r=2.5;
		final double PI = 3.1416;
		
		System.out.printf("Volumen W�rfel: %f\n",volumenWuerfel(a));
		System.out.printf("Volumen Quader: %f\n", volumenQuader(a,b,c));
		System.out.printf("Volumen Pyramide: %f\n",volumenPyramide(a,h));
		System.out.printf("Volumen Kugel: %f\n",volumenKugel(r,PI));
		
	}
		
		public static double volumenWuerfel (double a) 
		{
			return a*a*a;			
		}
		
		public static double volumenQuader (double a, double b, double c) {
			return a*b*c;			
		}
		public static double volumenPyramide (double a, double h) {
			return a*a*(h/3);			
		}
		public static double volumenKugel (double r, double pi) {
			return (4/3) *Math.pow(r, 3)*pi;			
		}

}
