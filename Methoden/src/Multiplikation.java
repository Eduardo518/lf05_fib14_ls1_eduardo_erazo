
public class Multiplikation {

	public static void main(String[] args) {
		
		double wert1 = 2.36;
		double wert2 = 7.87;
		
		System.out.print(wert1 +" * "+ wert2 + " = "+ multiplizieren(wert1,wert2));
		
	}
	
	public static double multiplizieren (double w1 , double w2) {
		double ergebniss = w1*w2;
		return ergebniss;
		
	}

}
